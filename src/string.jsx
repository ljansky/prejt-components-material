import React from 'react';

export default api => {

	const StringComponent = props => {
		return (<React.Fragment>{props.data.value}</React.Fragment>);
	};	

	return api.composePrejt(
		['value']
	)(StringComponent);
};