import { default as Box } from './box';
import { default as Button } from './button';
import { default as Layout } from './layout';
import { default as Menu } from './menu';
import { default as Text } from './text';
import { default as Grid } from './grid';
import { default as Col } from './col';
import { default as String } from './string';
import { default as Drawer } from './drawer';
import { default as Select } from './select';
import { default as Autocomplete } from './autocomplete';
import { default as Modal } from './modal';
import { default as ModalActions } from './modal-actions';
import { default as ModalContent } from './modal-content';
import { default as Date } from './date';
import { default as Tabs } from './tabs';
import { default as Toolbar } from './toolbar';
import { default as EditableGrid } from './editable-grid';
import { default as Separator } from './separator';

const components = {
	Box,
	Button,
	Layout,
	Menu,
	Text,
	Grid,
	Col,
	String,
	Drawer,
	Select,
	Autocomplete,
	Modal,
	ModalActions,
	ModalContent,
	Date,
	Tabs,
	Toolbar,
	EditableGrid,
	Separator
};

export default components;