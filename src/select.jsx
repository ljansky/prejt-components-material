import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import NativeSelect from '@material-ui/core/NativeSelect';

export default api => {

	const Wrap = props => {
		return (<FormControl>
			<InputLabel>{props.data.label}</InputLabel>
			<NativeSelect
				value={props.data && props.data.value || ''}
				onChange={props.onChange}
			>
				{props.children}
			</NativeSelect>
		</FormControl>);
	};

	const Item = api.composePrejt(
		['value', 'label']
	)(props => <option value={props.data.value}>{props.data.label}</option>);

	return api.composePrejt(
		['label'],
		api.withInput,
		api.withBase,
		api.withItems(Item)
	)(Wrap);
};