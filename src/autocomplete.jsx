import React from 'react';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import uuid from 'uuid';

export default api => {

	const styles = {
		container: {
			flex: '0 0 auto',
			position: 'relative',
		},
		paper: {
			position: 'absolute',
			zIndex: 1,
			marginTop: 0,
			left: 0,
			right: 0,
		},
	}

	class BaseAutocomplete extends React.Component {

		constructor(props) {
			super(props);

			this.setWrapperRef = this.setWrapperRef.bind(this);
			this.handleClickOutside = this.handleClickOutside.bind(this);
		}

		componentDidMount() {
			document.addEventListener('mousedown', this.handleClickOutside);
		}

		componentWillUnmount() {
			document.removeEventListener('mousedown', this.handleClickOutside);
		}

		setWrapperRef(node) {
			this.wrapperRef = node;
		}

		handleClickOutside(event) {
			if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
				this.props.resetSearch()
			}
		}

		render () {
			const { data, searchText, search, searching, children } = this.props;

			return (<React.Fragment>
				<TextField label={data.label} value={searchText} onChange={search} />
				{searching && (<div style={styles.container} ref={this.setWrapperRef}>
					<Paper style={styles.paper} square>{children}</Paper>
				</div>)}
			</React.Fragment>)
		}
	}

	const Item = props => {
		return (<MenuItem
			onClick={() => props.getWrapProps().onChange({ target: { value: props.data.value } })}
		>
			{props.data.label}
		</MenuItem>)
	}

	const AutocompleteItem = api.composePrejt(
		['value', 'label']
	)(Item);

	const PrejtAutocomplete = api.composePrejt(
		{ dataToProps: ['label', 'config', 'selected'] },
		api.withInput,
		api.withBase,
		api.withSubcomponentsInit,
		api.withAutocomplete,
		api.withItems(AutocompleteItem)
	)(BaseAutocomplete);

	return props => {
		const attributes = Object.assign({}, props.attributes, {
			config: props.attributes.config || `/autocompletes/${props.id}`
		})
		return <PrejtAutocomplete {...props} attributes={attributes} />
	}
}