// @flow

import React from 'react';
import TextField from '@material-ui/core/TextField';

export default api => {

	const TextComponent = props => {

		const inputProps = {}
		if (props.attributes.size) {
			inputProps.size = props.attributes.size
		}

		return <TextField
			label={props.data.label}
			value={props.data.value || ''}
			onChange={props.onChange}
			inputProps={inputProps}
		/>;
	}

	return api.composePrejt(
		['label'],
		api.withInput,
		api.withBase
	)(TextComponent);
}