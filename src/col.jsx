import React from 'react';
import TableCell from '@material-ui/core/TableCell';

export default api => {

	const WrapComponent = props => <TableCell>{props.children}</TableCell>;

	const Col = props => {
		return (<api.Container {...props} id={props.id} childConfigs={props.childConfigs} Wrap={WrapComponent} />);
	};	

	return api.composePrejt(
		['label'],
		api.withBase
	)(Col);
};