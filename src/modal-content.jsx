import React from 'react';
import DialogContent from '@material-ui/core/DialogContent';

export default api => {

	const ModalContent = props => {
		return (<DialogContent>
			<api.Container {...props} id={props.id} childConfigs={props.childConfigs} Wrap={api.DefaultWrap} />
		</DialogContent>);
	};

	return api.composePrejt(
		api.withBase,
		api.withLayout
	)(ModalContent);
};