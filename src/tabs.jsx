import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import SwipeableViews from 'react-swipeable-views';
import Paper from '@material-ui/core/Paper';

export default api => {

	class TabsComponent extends React.Component {

		constructor(props) {
			super(props);
			this.state = {
				selected: 0
			}

			this.select = this.select.bind(this);
			this.handleChangeIndex = this.handleChangeIndex.bind(this);
		}

		select (event, selected) {
			this.setState({
				selected
			})
		}

		handleChangeIndex (selected) {
			this.setState({
				selected
			})
		}

		render () {
			
			const { selected } = this.state;

			const tabs = this.props.childConfigs ? this.props.childConfigs.filter(it => it.name === 'tab') : []
			
			return (<Paper style={this.props.getStyles && this.props.getStyles()} elevation={1}>
					<AppBar position="static">
		        <Tabs value={selected} onChange={this.select}>
		        	{tabs.map((tab, index) => <Tab key={index} label={tab.attributes.label} />)}
		        </Tabs>
		      </AppBar>
		      
		      <SwipeableViews
	          axis='x'
	          index={this.state.selected}
	          onChangeIndex={this.handleChangeIndex}
	        >
			      {tabs.map((tab, index) => (
			      	<React.Fragment key={index}>
			      		{index === selected && <api.Container id={this.props.id} childConfigs={tabs[selected].childConfigs} />}
		      		</React.Fragment>
	      		))}
      		</SwipeableViews>
	  		</Paper>
	    )
		}
	}


	return api.composePrejt(
		['label'],
		api.withBase,
		api.withLayout
	)(TabsComponent);
};