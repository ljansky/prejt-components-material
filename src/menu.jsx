import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

export default api => {

	const Wrap = props => {
		return (<Drawer variant="permanent">
			<List component="nav">
				{props.children}
			</List>
		</Drawer>);
	};

	const Item = props => {
		return (<ListItem button>
			<ListItemText primary={props.data.label} onClick={() => props.callAction(props.attributes.click, props)} />
		</ListItem>);
	}

	const MenuItem = api.composePrejt(
		['label'],
		api.withAction,
		api.withBase
	)(Item);

	return api.composePrejt(
		api.withBase,
		api.withItems(MenuItem)
	)(Wrap);
};