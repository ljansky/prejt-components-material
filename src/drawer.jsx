import React from 'react';
import Drawer from '@material-ui/core/Drawer';

const paperProps = {
	style: {width: '50%'}
}

const WrapComponent = props => (<Drawer anchor="right" open={!!props.data.visible} PaperProps={paperProps}>
	{props.children}
</Drawer>);

export default api => {

	const DrawerComponent = props => {
		return (<api.Container {...props} id={props.id} childConfigs={props.childConfigs} Wrap={WrapComponent} />);
	};

	return api.composePrejt(
		['visible'],
		api.withBase,
		api.withLayout
	)(DrawerComponent);
};