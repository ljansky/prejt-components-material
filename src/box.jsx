import React from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
	root: Object.assign({}, theme.mixins.gutters(), {
		paddingTop: theme.spacing.unit * 2,
		paddingBottom: theme.spacing.unit * 2
	})
});

export default api => {

	const PaperComponent = props => (<Paper style={props.getStyles && props.getStyles()} className={props.classes.root} elevation={1}>
		<Typography variant="headline" component="h3">
			{props.data.label}
		</Typography>
		{props.children}
	</Paper>);

	const StyledPaperComponent = withStyles(styles)(PaperComponent);

	const Box = props => {
		return (<api.Container {...props} id={props.id} childConfigs={props.childConfigs} Wrap={StyledPaperComponent} />);
	};	

	return api.composePrejt(
		['label'],
		api.withBase,
		api.withLayout
	)(Box);
};