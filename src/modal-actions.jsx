import React from 'react';
import DialogActions from '@material-ui/core/DialogActions';

export default api => {

	const ModalActions = props => {
		return (<DialogActions>
			<api.Container {...props} id={props.id} childConfigs={props.childConfigs} Wrap={api.DefaultWrap} />
		</DialogActions>);
	};

	return api.composePrejt(
		api.withBase
	)(ModalActions);
};