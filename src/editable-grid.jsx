import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';

export default api => {

	const Wrap = props => {
    const item = props.childConfigs.find(it => it.name === 'item');
    const childConfigs = item && item.childConfigs && item.childConfigs.filter(col => (typeof col.attributes.editable === 'undefined' || col.attributes.editable === 'false')) || [];
		const hasHead = childConfigs.some(col => col.attributes.label);
		return (
			<Table>
			{hasHead && <TableHead>
        <TableRow>
          {childConfigs.map((col, index) => <TableCell key={index}>{col.attributes.label}</TableCell>)}
          <TableCell></TableCell>
        </TableRow>
      </TableHead>}
			<TableBody>
				{props.children}
			</TableBody>
		</Table>);
	};

  class Item extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        editable: false
      }

      this.toggleEditable = this.toggleEditable.bind(this);
    }

    toggleEditable () {
      this.setState(prevState => ({
        editable: !prevState.editable
      }))
    }

    render () {
      const { callAction, data: { resource, items, ['related-id']: relatedId }, attributes } = this.props.getWrapProps();
      // TODO: should be done better
      const dataPathId = this.props.dataPath[this.props.dataPath.length - 1];
      const itemId = items[dataPathId] && items[dataPathId].id;
      const editable = !itemId || this.state.editable;

      const childConfigs = this.props.childConfigs.filter(item => (typeof item.attributes.editable === 'undefined' || item.attributes.editable == String(editable)))

      if (!itemId) {
        childConfigs.push({
          name: 'set-value',
          attributes: {
            value: `./${relatedId}`,
            from: attributes['related-value']
            // from: '../../id'
          }
        })
      }

      const actionButtons = [];
      if (itemId) {
        if (editable) {
          actionButtons.push(<IconButton onClick={() => {
            callAction(`put(resource/${resource}/:id)|uuid(../../_/uuid)`, this.props);
            this.toggleEditable();
          }}>
            <Icon className={`fa fa-save`} fontSize={'medium'} />
          </IconButton>);
        } else {
          actionButtons.push(<IconButton onClick={this.toggleEditable}>
            <Icon className={`fa fa-edit`} fontSize={'medium'} />
          </IconButton>);
        }

        actionButtons.push(<IconButton onClick={() => callAction(`modal(modals/confirm)|delete(resource/${resource}/:id)|uuid(../../_/uuid)`, this.props)}>
          <Icon className={`fa fa-trash`} fontSize={'medium'} />
        </IconButton>);
      } else {
        actionButtons.push(<IconButton onClick={() => {
          callAction(`post(resource/${resource})|uuid(../../_/uuid)`, this.props);
        }}>
          <Icon className={`fa fa-save`} fontSize={'medium'} />
        </IconButton>)
      }

      return (<TableRow>
        <api.Container {...this.props} id={this.props.id} childConfigs={childConfigs} />
        <TableCell>
          {actionButtons}
        </TableCell>
      </TableRow>)
    }
  }

	return api.composePrejt(
		['resource', 'related-id'],
    api.withBase,
    api.withAction,
		api.withItems(Item)
	)(Wrap);
};