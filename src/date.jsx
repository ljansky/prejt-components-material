// @flow

import React from 'react';
import TextField from '@material-ui/core/TextField';

export default api => {

	const DateComponent = props => {
		const date = props.data.value ? new Date(props.data.value) : new Date()

		date.setMinutes(date.getMinutes() - date.getTimezoneOffset());

		const value = date.toJSON().slice(0, 10);
		return <TextField type='date' label={props.data.label} value={value} onChange={props.onChange} />;
	}

	return api.composePrejt(
		['label'],
		api.withInput,
		api.withBase
	)(DateComponent);
}