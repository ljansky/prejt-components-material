import React from 'react';

import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import { withStyles } from '@material-ui/core/styles';
import { lighten } from '@material-ui/core/styles/colorManipulator';
import classNames from 'classnames';

const toolbarStyles = theme => ({
  root: {
    paddingRight: theme.spacing.unit,
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  spacer: {
    flex: '1 1 100%',
  },
  actions: {
    color: theme.palette.text.secondary,
  },
  title: {
    flex: '0 0 auto',
  },
});

let EnchancedToolbar = props => {
	const { classes } = props;
	return (
		<Toolbar
      className={classNames(classes.root)}
    >
    	{props.children}
    </Toolbar>
	)
}

EnchancedToolbar = withStyles(toolbarStyles)(EnchancedToolbar)

export default api => {

	const Box = props => {
		return (<api.Container {...props} id={props.id} childConfigs={props.childConfigs} Wrap={EnchancedToolbar} />);
	};	

	return api.composePrejt(
		['label'],
		api.withBase,
		api.withLayout
	)(Box);
};