import React from 'react';

export default api => {

	const Layout = props => {
		return (<api.Container {...props} id={props.id} childConfigs={props.childConfigs} Wrap={api.DefaultWrap} />);
	};

	return api.composePrejt(
		api.withBase,
		api.withLayout
	)(Layout);
};