import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default api => {

	const BaseModal = props => {
		return (
			<Dialog
          open={!!props.data.enabled}
          onClose={() => props.callAction(props.attributes.close || `closeModal`, props)}
          fullScreen={false}
          scroll={'paper'}
          maxWidth={props.moduleAttributes && props.moduleAttributes.width || false}
          fullWidth={props.moduleAttributes && props.moduleAttributes.width}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
        	<DialogTitle>{props.moduleAttributes && props.moduleAttributes.label || ''}</DialogTitle>
          <api.Container {...props} id={props.id} childConfigs={props.childConfigs} />
				</Dialog>
		);
	};	

	return api.composePrejt(
		['enabled', 'module'],
		api.withAction
	)(BaseModal);
};