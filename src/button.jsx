import React from 'react';
import MaterialButton from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { withStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';
import 'font-awesome/css/font-awesome.css';
import Tooltip from '@material-ui/core/Tooltip';

const styles = theme => ({
	root: Object.assign({}, theme.mixins.gutters(), {
		margin: theme.spacing.unit
	})
});

export default api => {

	const Button = props => {
		const icon = props.data.icon ? <Icon className={`fa fa-${props.data.icon}`} fontSize={props.attributes.size || 'medium'} /> : null;
		const { variant } = props.attributes;
		const showTooltip = props.data.label && (variant === 'icon' || variant === 'fab');

		const button = variant === 'icon' ? (
			<IconButton
				color='primary'
				onClick={() => props.callAction(props.attributes.click, props)}
				className={props.classes.root}>
					{icon}
				</IconButton>
		) : (
			<MaterialButton
				color='primary'
				size={props.attributes.size || 'medium'}
				variant={props.attributes.variant || 'contained'}
				onClick={() => props.callAction(props.attributes.click, props)}
				className={props.classes.root}>
					{icon} {variant !== 'fab' && props.data.label}
			</MaterialButton>
		)

		return showTooltip ? (<Tooltip title={props.data.label}>
			{button}
		</Tooltip>) : button;
	};	

	const StyledButton = withStyles(styles)(Button);

	return api.composePrejt(
		['label', 'icon'],
		api.withAction,
		api.withBase
	)(StyledButton);
};