import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';



export default api => {

	const Wrap = props => {
		const item = props.childConfigs.find(it => it.name === 'item');
		const hasHead = item && item.childConfigs && item.childConfigs.some(col => col.attributes.label);
		return (
			<Table>
			{hasHead && <TableHead>
        <TableRow>
          {item && item.childConfigs.map((col, index) => <TableCell key={index}>{col.attributes.label}</TableCell>)}
        </TableRow>
      </TableHead>}
			<TableBody>
				{props.children}
			</TableBody>
		</Table>);
	};

	const WrapComponent = props => (<TableRow>{props.children}</TableRow>);

	const Item = props => {
		return (<api.Container {...props} id={props.id} childConfigs={props.childConfigs} Wrap={WrapComponent} />);
	}

	return api.composePrejt(
		['label'],
		api.withBase,
		api.withItems(Item)
	)(Wrap);
};